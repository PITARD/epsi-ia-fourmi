import csv
import networkx as nx


def get_id_from_libelle(libelle, data):
    for x in data:
        if libelle == x[1]:
            return x[0]
    else:
        return 0


def get_distance(id, data):
    for x in data:
        if id == x[0]:
            return x[2]
    else:
        return


def get_pheromone(id, data):
    for x in data:
        if id == x[0]:
            return x[5]
    else:
        return

# fonction d'evaporation qui enleve sur tout les edges 0.1 de pheromones
def evaporation(data):
    for x in data:
        if not x[5] == 0:
            x[5] = x[5] - 0.1

# calcul la longuer d'une rue en focntion du numéro des maisons.
def calcul_distance(bi_min, bi_max, bp_min, bp_max):
    if not bp_max == '' and not bi_min == '':
        return int(bp_max) - int(bi_min)
    if not bi_max == '' and not bp_min == '':
        return int(bi_max) - int(bp_min)
    if not bp_max == '' and not bp_min == '':
        return int(bp_max) - int(bp_min)
    if not bi_max == '' and not bi_min == '':
        return int(bi_max) - int(bi_min)

# a partir d'un CSV recup des données et formattae pour l'utilisation future
def populate_data(path_csv):
    data = []
    with open(path_csv, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for identifiant, row in enumerate(spamreader, 1):
            # "CATEGORIE","LIBELLE","MOT_DIRECTEUR","STATUT","COMMUNE","RIVOLI","TENANT","ABOUTISSANT","BI_MIN","BP_MIN","BI_MAX","BP_MAX"
            row_data = []
            if row[4] == "BASSE-GOULAINE":
                row_data.append(identifiant)
                row_data.append(row[4] + ' ' + row[1])
                row_data.append(calcul_distance(row[8],row[10],row[9],row[11]))
                row_data.append(row[6])
                row_data.append(row[7])
                row_data.append(0)
                data.append(row_data)
    return data


def create_graph(data):
    G = nx.Graph()
    # create all edges and node
    for row in data:
        tenant = get_id_from_libelle(row[3], data)
        if tenant == 0:
            tenant = row[0]
        abouttissant = get_id_from_libelle(row[4], data)
        if abouttissant == 0:
            abouttissant = row[0]
        G.add_edge(tenant, abouttissant, weight = row[2])
    pos = nx.spring_layout(G)
    nx.draw(G, pos)
    #set label in each edge
    for row in data:
        tenant = get_id_from_libelle(row[3], data)
        if tenant == 0:
            tenant = row[0]
        abouttissant = get_id_from_libelle(row[4], data)
        if abouttissant == 0:
            abouttissant = row[0]
        nx.draw_networkx_edge_labels(G, pos, edge_labels={(tenant, abouttissant): row[1]})
    return G