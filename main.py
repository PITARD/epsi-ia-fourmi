from method import populate_data, create_graph, get_id_from_libelle

import matplotlib as mpl
#lib pour afficher si on est dans un virtual env
mpl.use("TkAgg");


data = populate_data('VOIES_NM.csv')
graph = create_graph(data)

# affichage du graph
mpl.pyplot.show()

start = get_id_from_libelle("BASSE-GOULAINE Impasse du Muguet", data)
end = get_id_from_libelle("BASSE-GOULAINE Rue de Provence", data)



